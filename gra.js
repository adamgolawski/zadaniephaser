class mainScene {
	
	preload() {
		this.load.image('gracz', 'obrazki/U.png');
		this.load.image('dwojka', 'obrazki/2.png');
		this.load.image('trojka', 'obrazki/3.png');
	}

	create() {
		this.gracz = this.physics.add.sprite(100, 200, 'gracz');
		this.trojka = this.physics.add.sprite(300, 400, 'trojka');
		this.dwojka1 = this.physics.add.sprite(400, 30, 'dwojka');
		this.dwojka2 = this.physics.add.sprite(30, 150, 'dwojka');
		this.dwojka3 = this.physics.add.sprite(200, 470, 'dwojka');
		this.dwojka4 = this.physics.add.sprite(470, 360, 'dwojka');
		
		this.dwojka1WDol = true;
		this.dwojka1WGore = false;
		this.dwojka2WPrawo = true;
		this.dwojka2WLewo = false;
		this.dwojka3WDol = true;
		this.dwojka3WGore = false;
		this.dwojka4WPrawo = false;
		this.dwojka4WLewo = true;

		this.ileZostalo = 10;
		this.iloscZyc = 3;
		let stylLiczniki = { font: '20px Arial', fill: '#fff' };
		let stylKoniecGry = { font: '40px Arial', fill: '#FF0000'};
		this.ileZostaloTekst = this.add.text(10, 10, 'Zostało do złapania: ' + this.ileZostalo, stylLiczniki);
		this.iloscZycTekst = this.add.text(10, 50, 'Zostało żyć: ' + this.iloscZyc, stylLiczniki);
		this.wygralesTekst =  this.add.text(20, 230, '', stylKoniecGry);
		this.przegralesTekst =  this.add.text(20, 230, '', stylKoniecGry);

		this.strzalki = this.input.keyboard.createCursorKeys();
	}

	update() {
		if (this.physics.overlap(this.gracz, this.trojka)) {
			if(this.ileZostalo == 1){
				this.ileZostalo = 0;
				this.ileZostaloTekst.setText('Zostało do złapania: ' + this.ileZostalo);
				this.wygralesTekst.setText('Wygrałeś grę. Gratulacje!');
				this.trojka.visible = false;
				this.zatrzymajDwojki();
			}
			if(this.ileZostalo != 0)
				this.zlapanaTrojka();
		}
		if (this.physics.overlap(this.gracz, this.dwojka1)) {
			this.utrataZycia();
			this.dwojka1.y = 20;
			this.dwojka1WDol = true;
			this.dwojka1WGore = false;
		}
		if (this.physics.overlap(this.gracz, this.dwojka2)) {
			this.utrataZycia();
			this.dwojka2.x = 20;
			this.dwojka2WPrawo = true;
			this.dwojka2WLewo = false;
		}
		if (this.physics.overlap(this.gracz, this.dwojka3)) {
			this.utrataZycia();
			this.dwojka3.y = 480;
			this.dwojka3WDol = false;
			this.dwojka3WGore = true;
		}
		if (this.physics.overlap(this.gracz, this.dwojka4)) {
			this.utrataZycia();
			this.dwojka4.x = 480;
			this.dwojka4WPrawo = false;
			this.dwojka4WLewo = true;
		}
		
		if (this.strzalki.right.isDown){
			this.gracz.x += 3;
		} else if (this.strzalki.left.isDown){
			this.gracz.x -= 3;
		} 

		if (this.strzalki.down.isDown) {
			this.gracz.y += 3;
		} else if (this.strzalki.up.isDown){
			this.gracz.y -= 3;
		} 
		
		if(this.dwojka1WDol)
			this.dwojka1.y += 3;
		if(this.dwojka1WGore)
			this.dwojka1.y -= 3;
		if(this.dwojka1.y > 490){
			this.dwojka1WGore = true;
			this.dwojka1WDol = false;
			this.dwojka1.y -= 3;
		}
		if(this.dwojka1.y < 10){
			this.dwojka1WGore = false;
			this.dwojka1WDol = true;
			this.dwojka1.y += 3;
		}
		
		if(this.dwojka2WPrawo)
			this.dwojka2.x += 3;
		if(this.dwojka2WLewo)
			this.dwojka2.x -= 3;
		if(this.dwojka2.x > 490){
			this.dwojka2WLewo = true;
			this.dwojka2WPrawo = false;
			this.dwojka2.x -= 3;
		}
		if(this.dwojka2.x < 10){
			this.dwojka2WLewo = false;
			this.dwojka2WPrawo = true;
			this.dwojka2.x += 3;
		}
		
		if(this.dwojka3WDol)
			this.dwojka3.y += 3;
		if(this.dwojka3WGore)
			this.dwojka3.y -= 3;
		if(this.dwojka3.y > 490){
			this.dwojka3WGore = true;
			this.dwojka3WDol = false;
			this.dwojka3.y -= 3;
		}
		if(this.dwojka3.y < 10){
			this.dwojka3WGore = false;
			this.dwojka3WDol = true;
			this.dwojka3.y += 3;
		}

		if(this.dwojka4WPrawo)
			this.dwojka4.x += 3;
		if(this.dwojka4WLewo)
			this.dwojka4.x -= 3;
		if(this.dwojka4.x > 490){
			this.dwojka4WLewo = true;
			this.dwojka4WPrawo = false;
			this.dwojka4.x -= 3;
		}
		if(this.dwojka4.x < 10){
			this.dwojka4WLewo = false;
			this.dwojka4WPrawo = true;
			this.dwojka4.x += 3;
		}		
		if(this.iloscZyc < 1){
			this.zatrzymajDwojki();
			this.przegralesTekst.setText('Przegrałeś. Koniec gry!');
			this.trojka.visible = false;
			this.strzalki = "";
		}
		if(this.ileZostalo == 0){
			this.strzalki = "";
		}
		
	}
  
	zlapanaTrojka() {
		this.trojka.x = Phaser.Math.Between(50, 450);
		this.trojka.y = Phaser.Math.Between(50, 450);

		this.ileZostalo -= 1;
		this.ileZostaloTekst.setText('Zostało do złapania: ' + this.ileZostalo);
	}
	
	zatrzymajDwojki(){
		this.dwojka1WDol = false;
		this.dwojka1WGore = false;
		this.dwojka2WPrawo = false;
		this.dwojka2WLewo = false;
		this.dwojka3WDol = false;
		this.dwojka3WGore = false;
		this.dwojka4WPrawo = false;
		this.dwojka4WLewo = false;
	}
	
	utrataZycia(){
		this.iloscZyc -= 1;
		this.iloscZycTekst.setText('Zostało żyć: ' + this.iloscZyc);
		
	}
	
	
}

new Phaser.Game({
	width: 500,
	height: 500,
	backgroundColor: '#000066',
	scene: mainScene,
	physics: { default: 'arcade' },
	parent: 'gra',
});